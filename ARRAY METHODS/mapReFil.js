var members =
[
  {
    id: 111,
    name: "Chelsea Foster",
    workExp: 7,
    deptExp: 6,
    isPermanent: true,
  },
  {
    id: 102,
    name: "Aggie Sparling",
    workExp: 13,
    deptExp: 10,
    isPermanent: false,
  },
  {
    id: 123,
    name: "Timmy Matthews",
    workExp: 23,
    deptExp: 15,
    isPermanent: false,
  },
  {
    id: 66,
    name: "Emmet Foster",
    workExp: 22,
    deptExp: 9,
    isPermanent: true,
  },
  {
    id: 89,
    name: "Brent Dolan",
    workExp: 16,
    deptExp: 12,
    isPermanent: true,
  },
];


let permMembers = members.filter(function(member){
    return member.isPermanent
})
alert("permMembers",permMembers)

var totalScore = permMembers.map(function(totalexp){
    return totalexp.workExp + totalexp.deptExp
})

alert(totalScore)

let combineCotalScore = totalScore.reduce(function(acc, total){
    return acc + total
})

alert(combineCotalScore)