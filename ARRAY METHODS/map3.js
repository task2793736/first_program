var members =
[

  {

    id: 111,
    name: "Chelsea Foster",
    position: "Intern",

  },

  {

    id: 102,
    name: "Aggie Sparling",
    position: "Employee",

  },

  {

    id: 123,
    name: "Timmy Matthews",
    position: "Intern",
  },

  {

    id: 66,
    name: "Emmet Foster",
    position: "Employee",

  }

];

const interns = members.filter(member => member.faction === "Intern");
const employee = members.filter(member => member.faction === "Employee");