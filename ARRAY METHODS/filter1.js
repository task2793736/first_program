let army = {
    minAge: 18,
    maxAge: 27,
    canJoin(users){
        return users.age >= this.minAge && users.age < this.maxAge
    }

}

let users =[
    {age: 16},
    {age: 17},
    {age: 18},
    {age: 22},
    {age: 27},
    {age: 24},
    {age: 25},
    {age: 26},
    {age: 28},
    {age: 30},
]

let soldier  = users.filter(army.canJoin, army)

alert(soldier.length)
alert(soldier[0].age)
alert(soldier[1].age)
alert(soldier[3].age)

