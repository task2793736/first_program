// api url
const api_url =
	"https://jsonplaceholder.typicode.com/todos";
let datas = [];
// Defining async function
async function getapi(url) {

	// Storing response
	const response = await fetch(url);

	// Storing data in form of JSON
	var data = await response.json();
	console.log('datas',data);
	if (response) {
		hideloader();
	}
    datas = data;
	show(data);
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
	document.getElementById('loading').style.display = 'none';
}
// Function to define innerHTML for HTML table
function show(curentData) {
    console.log("curentData",curentData);
	let tab =
		`<tr>
		<th>userId</th>
		<th>id</th>
		<th>title</th>
		<th>completed</th>
		</tr>`;

	// Loop to access all rows
	for (let r of curentData ) {
		tab += `<tr>
	<td>${r.userId} </td>
	<td>${r.id}</td>
	<td>${r.title}</td>
	<td>${r.completed}</td>		
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("employees").innerHTML = tab;
}
var newarray = [];
document.getElementById('inputText').addEventListener('keyup', function (e) {
    var inputText = this.value.trim(); // Use this.value instead of this.inputText.value
    console.log("inputText", inputText);
    newarray = datas.filter(function (val) {
        // Convert val properties to lowercase for a case-insensitive search
        var id = val.id;
        var userId = val.userId;
        var title = val.title.toLowerCase();
        // Check if any property contains the inputText
        return id == inputText || userId == inputText || title.includes(inputText);
    });

    console.log("newarray", newarray);

    show(newarray);
});


// var newarray = []
// document.getElementById('inputText').addEventListener('keyup', function(){
//     var inputText = this.inputText.value.toLowerCase();
//     console.log("inputText",inputText);
//     newarray = data.filter(function (val){
//         console.log("val",val);
//         if(val.id.includes(inputText)|| val.userId.includes(inputText) || val.title.includes(inputText) || val.completed.includes(inputText)){
//             newobj = {id:val.id, userId:val.userId, title:val.title, completed:val.completed, }
//             return newobj
//         }
//     })
//     console.log("newarray",newarray);

//     show(newarray)
// });

