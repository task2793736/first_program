let a = 12;
let b = 18;

while (b !== 0) {
    const temp = b;
    b = a % b;
    a = temp;
}

const hcf = a;
const lcm = (12 * 18) / hcf;

console.log("LCM of 12 and 18 is",lcm);
